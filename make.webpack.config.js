const HtmlPlugin = require('html-webpack-plugin');

const config = {
  entry: [
    './src/index.jsx'
  ],
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader']
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  plugins: [
    new HtmlPlugin({
      template: './src/index.html'
    })
  ],
  output: {
    path: __dirname + '/build',
    publicPath:
      '/',
    filename:
      'bundle.js'
  }
};

module.exports = function (overrides) {
  return Object.assign(config, overrides);
};